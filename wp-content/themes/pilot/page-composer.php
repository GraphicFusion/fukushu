<?php
/**
 * Template Name: Composer
 */

get_header( 'composer' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
	<!-- content -->
    <section class="content">
        <?php get_all_blocks('composer', false); ?>
    </section>

<?php

endwhile; endif;

get_footer();