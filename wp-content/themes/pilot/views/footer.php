<!-- footer -->
<footer id="footer" class="main_footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <?php if ( get_theme_mod( 'footer_logo', false ) ) : ?>
                <a class="footer_logo" href="<?php echo home_url(); ?>">
                    <img src="<?php echo get_template_directory_uri() . '/image/fukushu-footer.svg'; ?>">
                </a>
                <?php endif; ?>
            </div>
            <div class="col-sm-8">
                <div class="row">
                    <?php dynamic_sidebar( 'footer-left-sidebar' ); ?>

                    <?php dynamic_sidebar( 'footer-middle-sidebar' ); ?>

                    <?php dynamic_sidebar( 'footer-right-sidebar' ); ?>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p class="copyright">
                            &copy; <?php the_date( 'Y' ); ?>
                            <?php
                                if ( get_theme_mod( 'copyright_textbox' ) ) :
                                    echo get_theme_mod( 'copyright_textbox', '' );
                                else :
                                    echo "FUKUSHU RESTAURANT CONCEPTS. ALL RIGHTS RESERVED. DESIGN BY SONDER";
                                endif;
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_img"></div>
</footer>
