<div class="header_outer" id="header">
  <header class="main_header">
      <div class="container">
          <div class="row">
              <div class="col-sm-12">
                  <nav class="navbar navbar-default">
                    <div class="container-fluid">
                      <!-- Brand and toggle get grouped for better mobile display -->
                      <div class="navbar-header">
                        <a class="navbar-toggle collapsed" href="javascript:void(0);" onclick="ScrollToPosNormal('#footer')">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </a>

                        <?php if ( is_front_page() ) : ?>
                        <a class="navbar-brand" href="javascript:void(0)" onclick="ScrollToPos('#header')">
                            <img src="<?php echo get_theme_mod( 'header_logo', '' ); ?>">
                        </a>
                        <?php else : ?>
                          <a class="navbar-brand" href="<?php echo home_url(); ?>">
                            <img src="<?php echo get_theme_mod( 'header_logo', '' ); ?>">
                          </a>
                      <?php endif; ?>

                      </div>

                      <!-- Collect the nav links, forms, and other content for toggling -->
                      <?php
                          wp_nav_menu( array(
                              'theme_location' => 'primary',
                              'container' => 'div',
                              'container_id' => 'bs-example-navbar-collapse-1',
                              'container_class' => 'collapse navbar-collapse',
                              'menu_class' => 'nav navbar-nav navbar-right'
                          ) );
                      ?>
                    </div><!-- /.container-fluid -->
                  </nav>
              </div>
          </div>
      </div>
  </header>
</div>