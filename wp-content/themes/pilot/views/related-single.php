<?php

$cat = pilot_get_first_category( 'id' );
// echo "<pre>";
// var_dump( $cat );
// die();

if ( $cat != null ) :
    $related_posts = pilot_related_by_category( $cat );

    if ( count( $related_posts ) ) :
?>
<div class="row">
    <div class="col-sm-12">
        <div class="morenews_btn">
            <a href="<?php the_permalink( get_option( 'page_for_posts', true ) ); ?>">More News</a>
        </div>
    </div>
</div>
<div class="row">
    <?php foreach ( $related_posts as $related ) : /*echo "<pre>"; print_r( $related ); echo "</pre>";*/ ?>
    <div class="col-sm-6">
        <div class="morenews_box">
            <a href="<?php echo $related[ 'permalink' ]; ?>">
                <img src="<?php echo $related[ 'thumbnail' ]; ?>" alt="<?php echo $related[ 'title' ]; ?>" class="img-responsive">
                
                <div class="morenews_inner">
                    <h5><?php echo $related[ 'category' ]; ?> - <?php echo $related[ 'date' ]; ?></h5>
                    <h4><?php echo $related[ 'title' ]; ?></h4>
                </div>
            </a>
        </div>
    </div>
    <?php endforeach; ?>
</div>
<?php endif; endif; ?>