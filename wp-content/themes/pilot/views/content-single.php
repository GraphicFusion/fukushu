<div class="row">
    <div class="col-sm-12">
        <h3><?php echo pilot_get_first_category(); ?> - <?php echo get_the_date( 'm/d/Y' ); ?></h3>  
        <h2><?php echo pilot_get_title(); ?></h2>
    </div>
</div>
<?php if ( has_post_thumbnail() ) : ?>
<div class="row">
    <div class="col-sm-12">
        <a href="<?php the_permalink(); ?>">
            <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="main_postthumbnail img-responsive">   
        </a>
    </div>
</div>
<?php endif; ?>
<div class="row">
    <div class="col-sm-12">
        <div class="single-post-content custom-content-inner">
            <?php the_content(); ?>
        </div>
    </div>
    <div class="col-sm-2"></div>
</div>