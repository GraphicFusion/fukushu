<div class="header_outer" id="header">
  <!-- main header -->
  <header class="sub_header">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <nav class="navbar navbar-default">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <a class="navbar-toggle collapsed" href="javascript:void(0)" onclick="ScrollToPosNormal( '#footer' );">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </a>
                
                <a class="navbar-brand" href="<?php echo home_url(); ?>">
                  <img src="<?php echo get_theme_mod( 'header_logo', '' ); ?>">
                </a>

              </div>

              <?php
                wp_nav_menu( array(
                    'theme_location' => 'primary',
                    'container' => 'div',
                    'container_id' => 'bs-example-navbar-collapse-1',
                    'container_class' => 'collapse navbar-collapse',
                    'menu_class' => 'nav navbar-nav navbar-right'
                ) );
              ?>
            </div>
          </nav>
        </div>
      </div>
      <?php if ( !is_singular( 'post' ) && !get_field( 'hide_title' ) ) : ?>
        <div class="row">
            <div class="col-sm-12">
                <h1>
                <?php
                  if ( !is_front_page() && is_home() ) {
                    echo get_the_title( get_option( 'page_for_posts', true ) );
                  } else if ( is_post_type_archive() ) {
                    post_type_archive_title();
                  } else if ( is_singular( 'career' ) ) {
                    echo "Career: " . get_field( 'position' );
                  } else {
                    the_title();
                  }
                ?>
            </div>
        </div>
      <?php endif; ?>
    </div>
  </header>
</div>