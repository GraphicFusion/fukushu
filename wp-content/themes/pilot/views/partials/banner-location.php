<div class="restaurant_location_banner <?php echo get_the_title( $restaurant_id ); ?>">
    <div class="restaurant_banner_inner" style="background-image: url(<?php the_post_thumbnail_url( 'full' ); ?>);"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="reservation pull-left">
                    Call for Reservations
                    <!-- a href="<?php the_field( 'reservations_link', $restaurant_id ); ?>"></a -->
                </div>
                <div class="obon_logo pull-left">
                    <a href="<?php the_permalink( $restaurant_id ); ?>">
                        <img src="<?php the_field( 'restaurant_header_logo', $restaurant_id ); ?>" alt="<?php echo get_the_title( $restaurant_id ); ?>">
                    </a>
                </div>
                <div class="sub_navbtn pull-right">
                    <span class="pull-left">More</span>
                    <img src="<?php echo get_template_directory_uri() . '/image/nav-icon.png'; ?>" class="pull-left">
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>