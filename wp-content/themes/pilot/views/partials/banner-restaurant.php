<!-- vision hero img -->
<section class="restaurant_heroimg">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            
            <?php
                if ( have_rows( 'slides' ) ) :
                    $first_slide = true;

                    while ( have_rows( 'slides' ) ) :
                        the_row();
            ?>
                <div class="item <?php echo $first_slide ? 'active' : ''; ?>">
                    <span class="slide_one" style="background-image: url(<?php the_sub_field( 'slide_background_image' ); ?>);"></span>
                    <div class="carousel-caption">
                    </div>
                </div>
            <?php
                    $first_slide = false;
                    endwhile;
                endif;
            ?>
            
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon" aria-hidden="true">
                <svg version="1.1" id="left_arrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40.75px" height="13.75px" viewBox="0 0 40.75 13.75" enable-background="new 0 0 40.75 13.75" xml:space="preserve">
                    <g>
                        <rect x="2.093" y="5.875" fill="currentColor" width="37.686" height="1.918"/>
                        <polyline fill="none" stroke="currentColor" stroke-width="2" stroke-miterlimit="10" points="7.153,0.877 1.532,6.875 7.153,12.874    
                            "/>
                    </g>
                </svg>
            </span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon" aria-hidden="true">
                <svg version="1.1" id="right_arrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40.75px" height="13.75px" viewBox="0 0 40.75 13.75" enable-background="new 0 0 40.75 13.75" xml:space="preserve">
                    <g>
                        <rect x="0.875" y="5.875" fill="currentColor" width="37.686" height="1.918"/>
                        <polyline fill="none" stroke="currentColor" stroke-width="2" stroke-miterlimit="10" points="33.5,0.877 39.121,6.875 33.5,12.874"/>
                    </g>
                </svg>
            </span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="reservation pull-left">
                    Call for Reservations
                    <?php //the_field( 'reservations_link' ); ?>
                </div>
                <div class="obon_logo pull-left">
                    <a href="<?php the_permalink(); ?>">
                        <img src="<?php the_field( 'restaurant_header_logo' ); ?>" alt="<?php the_title(); ?>">
                    </a>
                </div>
                <div class="sub_navbtn pull-right">
                    <span class="pull-left">more</span>
                    <img src="<?php echo get_template_directory_uri() . '/image/nav-icon.png'; ?>" class="pull-left">
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>