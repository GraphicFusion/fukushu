<?php
    wp_reset_query();

    $restaurant_id = get_the_ID();
    
    if ( is_singular( 'location' ) ) {
        $restaurant_id = get_field( 'restaurant' )->ID;
    }
?>
<!-- footer -->
<footer id="footer" class="sub_footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <a class="footer_logo" href="#">
                    <img src="<?php echo get_the_post_thumbnail_url( $restaurant_id ); ?>" alt="<?php echo get_the_title( $restaurant_id ); ?>">
                </a>
            </div>
            <div class="col-sm-8">
                <div class="row">
                    
                    <?php if ( have_rows( 'left_list_items', $restaurant_id ) ) : ?>
                    <div class="col-xs-4 footer_list">
                        <h6><?php the_field( 'left_list_title', $restaurant_id ); ?></h6>
                        <ul>
                            <?php while ( have_rows( 'left_list_items', $restaurant_id ) ) : the_row(); ?>
                                <li><a href="<?php the_sub_field( 'item_link' ); ?>"><?php the_sub_field( 'item_text' ); ?></a></li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                    <?php
                        else :
                            dynamic_sidebar( 'footer-left-sidebar' );
                        endif;
                    ?>

                    <?php if ( have_rows( 'middle_list_items', $restaurant_id ) ) : ?>
                    <div class="col-xs-4 footer_list">
                        <h6><?php the_field( 'middle_list_title', $restaurant_id ); ?></h6>
                        <ul>
                            <?php while ( have_rows( 'middle_list_items', $restaurant_id ) ) : the_row(); ?>
                                <li><a href="<?php the_sub_field( 'item_link' ); ?>"><?php the_sub_field( 'item_text' ); ?></a></li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                    <?php
                        else :
                            dynamic_sidebar( 'footer-middle-sidebar' );
                        endif;
                    ?>

                    <?php if ( have_rows( 'right_list_items', $restaurant_id ) ) : ?>
                    <div class="col-xs-4 footer_list">
                        <h6><?php the_field( 'right_list_title', $restaurant_id ); ?></h6>
                        <ul>
                            <?php while ( have_rows( 'right_list_items', $restaurant_id ) ) : the_row(); ?>
                                <li><a href="<?php the_sub_field( 'item_link' ); ?>"><?php the_sub_field( 'item_text' ); ?></a></li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                    <?php
                        else :
                            dynamic_sidebar( 'footer-right-sidebar' );
                        endif;
                    ?>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p class="copyright">
                            &copy; <?php the_date( 'Y' ); ?>
                            <?php
                                if ( get_theme_mod( 'copyright_textbox' ) ) :
                                    echo get_theme_mod( 'copyright_textbox', '' );
                                else :
                                    echo "FUKUSHU RESTAURANT CONCEPTS. ALL RIGHTS RESERVED. DESIGN BY SONDER";
                                endif;
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>