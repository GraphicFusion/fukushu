<h2><?php the_title(); ?></h2>

<!-- Description Start -->
<h3>Description</h3>
<?php the_field( 'description' ); ?>
<!-- Ddescription End -->

<!-- Bullet Sets Start -->
<?php while ( have_rows( 'skill_sets' ) ) : the_row(); ?>
<h3><?php the_sub_field( 'title' ); ?></h3>
<ul class="careers_inner_list">
    <?php while( have_rows( 'bullet_points' ) ) : the_row(); ?>
        <li><?php the_sub_field( 'point_content' ); ?></li>
    <?php endwhile ?>
</ul>
<div class="clearfix"></div>
<?php endwhile; ?>
<!-- Bullet Sets End -->

<h3>Applications</h3>
<?php the_field( 'applications' ); ?>