<div class="post_box">
    <a href="<?php the_permalink(); ?>">
        <img src="<?php the_post_thumbnail_url( 'index-thumbnail' ); ?>" alt="<?php the_title(); ?>" class="img-responsive">
        <div class="post_inner">
            <h4><?php echo pilot_get_first_category(); ?> - <?php echo get_the_date( 'm/d/Y' ); ?></h4>
            <h3><?php the_title(); ?></h3>
        </div>                                        
    </a>
</div>