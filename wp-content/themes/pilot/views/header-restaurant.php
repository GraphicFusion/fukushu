<?php
    $restaurant_id = get_the_ID();
    if ( is_singular( 'location' ) ) {
        $restaurant_id = get_field( 'restaurant' )->ID;
    }

    $primary_color = get_field( 'primary_color', $restaurant_id );
?>
<style>
    .restaurantintro_outer .dining_detail .diningdetail_row h4,
    .restaurant_newsevents .newevents_btn,
    .food_menu_outer .food_menu_title,
    .sub_nav,
    .sub_footer {
        background: <?php echo $primary_color; ?>
    }
    .restaurantintro_outer .restaurantintro_logo,
    .restaurantintro_outer .dining_detail .diningdetail_row .diningarrow_btn,
    .food_menu_outer .location_detail .menu_title_list li,
    .main_food_menu .food_menu_sidebar .food_menu_list li a,
    .restaurantintro_outer.locations_layout_2 .dinning_inner a.title,
    .restaurantintro_outer .dining_detail .diningdetail_row h4:hover {
        color: <?php echo $primary_color; ?>
    }

    .restaurantintro_outer .dining_detail .diningdetail_row h4:hover {
        background-color: #fff;
        border-color: <?php echo $primary_color; ?>;
    }

    /* facny slider nav arrows */
    .slider > .navContainer > .rightClass {
        background: url('data:image/svg+xml;utf8,<svg version="1.1" id="right_arrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40.75px" height="13.75px" viewBox="0 0 40.75 13.75" enable-background="new 0 0 40.75 13.75" xml:space="preserve"><g><rect x="0.875" y="5.875" fill="<?php echo $primary_color; ?>" width="37.686" height="1.918"/><polyline fill="none" stroke="<?php echo $primary_color; ?>" stroke-width="2" stroke-miterlimit="10" points="33.5,0.877 39.121,6.875 33.5,12.874"/></g></svg>') no-repeat center;
    }
    .slider > .navContainer > .leftClass {
        background: url( 'data:image/svg+xml;utf8,<svg version="1.1" id="left_arrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40.75px" height="13.75px" viewBox="0 0 40.75 13.75" enable-background="new 0 0 40.75 13.75" xml:space="preserve"><g><rect x="2.093" y="5.875" fill="<?php echo $primary_color; ?>" width="37.686" height="1.918"/><polyline fill="none" stroke="<?php echo $primary_color; ?>" stroke-width="2" stroke-miterlimit="10" points="7.153,0.877 1.532,6.875 7.153,12.874"/></g></svg>') no-repeat center;
    }
</style>
<div class="sub_nav">
    <div class="subnav_inner">
        <img src="<?php echo get_template_directory_uri() . '/image/nav-cross-btn.png'; ?>" class="nav_crossbtn pull-right">
        <div class="clearfix"></div>

        <?php if ( have_rows( 'restaurant_primary_menu', $restaurant_id ) ) : ?>
            <ul class="primary_list">
            <?php while ( have_rows( 'restaurant_primary_menu', $restaurant_id ) ) : the_row(); ?>
                <li><a href="<?php the_sub_field( 'menu_item_url' ); ?>"><?php the_sub_field( 'menu_item_text' ); ?></a></li>
            <?php endwhile; ?>
            </ul>
        <?php
            else :
                wp_nav_menu( array(
                  'theme_location' => 'restaurant-primary',
                  'container' => '',
                  'container_id' => '',
                  'container_class' => '',
                  'menu_class' => 'primary_list'
                ) );
            endif;
        ?>


        <?php if ( have_rows( 'restaurant_secondary_menu', $restaurant_id ) ) : ?>
            <ul class="secondary_list">
            <?php while ( have_rows( 'restaurant_secondary_menu', $restaurant_id ) ) : the_row(); ?>
                <li><a href="<?php the_sub_field( 'menu_item_url' ); ?>"><?php the_sub_field( 'menu_item_text' ); ?></a></li>
            <?php endwhile; ?>
            </ul>
        <?php
            else :
                wp_nav_menu( array(
                  'theme_location' => 'restaurant-secondary',
                  'container' => '',
                  'container_id' => '',
                  'container_class' => '',
                  'menu_class' => 'secondary_list'
                ) );
            endif;
        ?>
    </div>
</div>
<div class="restaurant_topborder">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2><a href="<?php echo home_url(); ?>"><?php echo get_bloginfo( 'name' ); ?></a></h2>
            </div>
        </div>
    </div>
</div>

<?php $restaurant_id = get_the_ID(); ?>

<?php
    if ( is_singular( 'location' ) ) :
        $restaurant_id = get_field( 'restaurant' )->ID;
        // get_template_part( 'views/partials/banner', 'location' );
        include( locate_template( 'views/partials/banner-location.php' ) );
    else :
        get_template_part( 'views/partials/banner', 'restaurant' );
    endif;
?>
<div class="clearfix"></div>