<?php get_header( 'restaurant' ); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<section class="content">
	    <?php get_all_blocks('location', false); ?>
	</section>
<?php endwhile; endif; ?>

<?php get_footer( 'restaurant' ); ?>