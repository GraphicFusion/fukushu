<?php
/**
 * Enqueue scripts and styles.
 */
function pilot_scripts() {

	// fonts
	wp_enqueue_style( 'font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' );
    
  wp_enqueue_style( 'pilot-style', get_template_directory_uri().'/dest/css/main.min.css' );

	wp_deregister_script('jquery');
	wp_register_script('jquery', ('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'), false, '2.1.4', true);

	wp_enqueue_script( 'pilot-libraries', get_template_directory_uri() . '/dest/js/lib.min.js', array('jquery'), '20120206', true );
	wp_enqueue_script( 'pilot-wow', get_template_directory_uri() . '/bower_components/wow/dist/wow.js', array('jquery'), '20120206', false );

	wp_enqueue_script( 'bootstrap-min', get_template_directory_uri() . '/src/js/bootstrap.min.js', array( 'jquery' ), NULL, true);

    wp_enqueue_script( 'pilot-scripts', get_template_directory_uri() . '/dest/js/app.min.js', array('jquery'), '20120206', true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// enqueue styles and scripts
	wp_localize_script( 'sorcery-scripts', 'ajax', array('ajaxurl' => admin_url('admin-ajax.php'),) );

	if ( is_front_page() ) {
		wp_enqueue_script( 'home-video', get_template_directory_uri() . '/src/js/home_video.js', array( 'jquery' ), NULL, true );
	}

	// slider script
	// if ( is_singular( 'location' ) ) {
	// 	wp_enqueue_script( 'slider.jquery', get_template_directory_uri() . '/src/js/slider.jquery.js', array( 'jquery' ), NULL, true );

	// 	wp_enqueue_style( 'fancy-slider', get_template_directory_uri() . '/src/sass/views/_fancy_slider.css' );
	// }

}
add_action( 'wp_enqueue_scripts', 'pilot_scripts' );

function load_maxwell_admin_style() {
        wp_register_style( 'maxwell_admin_css', get_template_directory_uri() . '/includes/modules/admin-modules.css', false, '1.0.0' );
        wp_enqueue_style( 'maxwell_admin_css' );
		wp_enqueue_script( 'maxwell_admin_js',  get_template_directory_uri() . '/includes/modules/admin-modules.js' , array('jquery'));
}
add_action( 'admin_enqueue_scripts', 'load_maxwell_admin_style' );