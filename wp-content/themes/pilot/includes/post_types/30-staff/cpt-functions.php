<?php

// Register Staff CPT
pilot_staff_cpt();
pilot_acf_staff_options_def();


function pilot_staff_cpt() {

	$labels = array(
		'name'                  => _x( 'Staff Members', 'Staff Members', 'pilot' ),
		'singular_name'         => _x( 'Staff Member', 'Staff Member', 'pilot' ),
		'menu_name'             => __( 'Staff Members', 'pilot' ),
		'name_admin_bar'        => __( 'Staff Member', 'pilot' ),
		'archives'              => __( 'Item Archives', 'pilot' ),
		'attributes'            => __( 'Item Attributes', 'pilot' ),
		'parent_item_colon'     => __( 'Parent Item:', 'pilot' ),
		'all_items'             => __( 'All Items', 'pilot' ),
		'add_new_item'          => __( 'Add New Item', 'pilot' ),
		'add_new'               => __( 'Add New', 'pilot' ),
		'new_item'              => __( 'New Item', 'pilot' ),
		'edit_item'             => __( 'Edit Item', 'pilot' ),
		'update_item'           => __( 'Update Item', 'pilot' ),
		'view_item'             => __( 'View Item', 'pilot' ),
		'view_items'            => __( 'View Items', 'pilot' ),
		'search_items'          => __( 'Search Item', 'pilot' ),
		'not_found'             => __( 'Not found', 'pilot' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'pilot' ),
		'featured_image'        => __( 'Featured Image', 'pilot' ),
		'set_featured_image'    => __( 'Set featured image', 'pilot' ),
		'remove_featured_image' => __( 'Remove featured image', 'pilot' ),
		'use_featured_image'    => __( 'Use as featured image', 'pilot' ),
		'insert_into_item'      => __( 'Insert into item', 'pilot' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'pilot' ),
		'items_list'            => __( 'Items list', 'pilot' ),
		'items_list_navigation' => __( 'Items list navigation', 'pilot' ),
		'filter_items_list'     => __( 'Filter items list', 'pilot' ),
	);
	$args = array(
		'label'                 => __( 'Staff', 'pilot' ),
		'description'           => __( 'Post Type Description', 'pilot' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'staff', $args );
}

function pilot_acf_staff_options_def() {

	if( function_exists('acf_add_local_field_group') ):

		acf_add_local_field_group(array (
			'key' => 'group_5922ef06d5d26',
			'title' => 'Career Options',
			'fields' => array (
				array (
					'key' => 'field_5922f0cc2a74d',
					'label' => 'Position Title',
					'name' => 'position',
					'type' => 'text',
					'instructions' => 'Note: Use post title for Single page\'s title.',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5922ef16c8a8b',
					'label' => 'Establishment',
					'name' => 'establishment',
					'type' => 'post_object',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'post_type' => array (
						0 => 'restaurant',
					),
					'taxonomy' => array (
					),
					'allow_null' => 0,
					'multiple' => 0,
					'return_format' => 'object',
					'ui' => 1,
				),
				array (
					'key' => 'field_5922ef50c8a8c',
					'label' => 'Location',
					'name' => 'location',
					'type' => 'text',
					'instructions' => 'Format: City, State',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5922f1012a74e',
					'label' => 'Description',
					'name' => 'description',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
				),
				array (
					'key' => 'field_5922f11c2a74f',
					'label' => 'Skill Sets',
					'name' => 'skill_sets',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => '',
					'min' => '',
					'max' => '',
					'layout' => 'table',
					'button_label' => 'Add Set',
					'sub_fields' => array (
						array (
							'key' => 'field_5922f1672a750',
							'label' => 'Title',
							'name' => 'title',
							'type' => 'text',
							'instructions' => '',
							'required' => 1,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
						array (
							'key' => 'field_5922f1872a751',
							'label' => 'Bullet Points',
							'name' => 'bullet_points',
							'type' => 'repeater',
							'instructions' => '',
							'required' => 1,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'collapsed' => '',
							'min' => '',
							'max' => '',
							'layout' => 'table',
							'button_label' => 'Add Point',
							'sub_fields' => array (
								array (
									'key' => 'field_5922f1a82a752',
									'label' => 'Point Text/Content',
									'name' => 'point_content',
									'type' => 'text',
									'instructions' => '',
									'required' => 1,
									'conditional_logic' => 0,
									'wrapper' => array (
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'default_value' => '',
									'placeholder' => '',
									'prepend' => '',
									'append' => '',
									'maxlength' => '',
								),
							),
						),
					),
				),
				array (
					'key' => 'field_5922f21ccc95d',
					'label' => 'Applications',
					'name' => 'applications',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'career',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => array (
				0 => 'the_content',
			),
			'active' => 1,
			'description' => '',
		));

	endif;

}