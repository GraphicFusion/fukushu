<?php
	/**
	 * string	$args['title']
	 * string	$args['cta_phrase']
	 * string	$args['introduction_logo']
	 */
	global $args;
?>
<div class="restaurantintro_outer">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <img src="<?php echo $args[ 'introduction_logo' ]; ?>" class="restaurantintro_logo">
            </div>
            <div class="col-sm-8">
                <h3 class="intro_title"><?php echo $args[ 'title' ]; ?></h3>
                <p class="intro_text"><?php echo $args[ 'cta_phrase' ]; ?></p>
            </div>
        </div>
    </div>                
</div>