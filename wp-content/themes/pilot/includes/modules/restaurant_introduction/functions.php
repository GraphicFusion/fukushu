<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if( file_exists ( $filename )){
    require $filename;
}

function build_restaurant_introduction_layout(){
    $args = array(
        'title' => get_sub_field( 'restaurant_introduction_block_title' ),
        'cta_phrase' => get_sub_field( 'cta_phrase' ),
        'introduction_logo' => get_sub_field( 'introduction_logo' )
    );

    return $args;
}
?>