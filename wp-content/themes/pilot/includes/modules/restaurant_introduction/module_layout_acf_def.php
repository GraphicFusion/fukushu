<?php
global $pilot;
// add module layout to flexible content
$module_layout = array (
    'key' => '5909fbcdcb0cc',
    'name' => 'restaurant_introduction',
    'label' => 'Restaurant Introduction',
    'display' => 'block',
    'sub_fields' => array (
        array (
            'key' => 'field_5909fc0fbf5c4',
            'label' => 'Introduction Logo',
            'name' => 'introduction_logo',
            'type' => 'image',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'url',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
        array (
            'key' => 'field_5909fbd2bf5c3',
            'label' => 'CTA Phrase',
            'name' => 'cta_phrase',
            'type' => 'text',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'min' => '',
    'max' => '',
);

?>