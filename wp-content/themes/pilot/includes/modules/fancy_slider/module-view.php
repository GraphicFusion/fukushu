<?php
	/**
	 * string	$args['title']
	 * array	$args['images']
	 */
	global $args;
?>
<div class="food_menu_outer">
    <div class="restaurant_gallery">
        <section class="slider">
            <?php
                $class_names = [ 'left', 'center', 'right' ];
                $i = 0;
                foreach( $args[ 'images' ] as $image ) :
            ?>
                <span width="100%" height="100%" class="<?php echo $i < 3 ? $class_names[ $i++ ] : ''; ?>">
                    <img src="<?php echo $image; ?>">
                </span>
            <?php endforeach; ?>
        </section>
    </div>
</div>