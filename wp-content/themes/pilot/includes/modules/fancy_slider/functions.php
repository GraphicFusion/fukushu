<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if( file_exists ( $filename )){
    require $filename;
}

function build_fancy_slider_layout(){
    $args = array(
		'title' => get_sub_field('fancy_slider_block_title'),
		'images' => ''
    );

    $images = array();

    while ( have_rows( 'images' ) ) : the_row();
    	$images[] = get_sub_field( 'image' );
    endwhile;

    $args[ 'images' ] = $images;
    return $args;
}
?>