<?php
global $pilot;
// add module layout to flexible content
$module_layout = array (
    'key' => '5923437baea8b',
    'name' => 'fancy_slider',
    'label' => 'Fancy Slider',
    'display' => 'block',
    'sub_fields' => array (
        array (
            'key' => 'field_59234380750eb',
            'label' => 'Images',
            'name' => 'images',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'collapsed' => '',
            'min' => '',
            'max' => '',
            'layout' => 'table',
            'button_label' => 'Add Row',
            'sub_fields' => array (
                array (
                    'key' => 'field_592343c3750ec',
                    'label' => 'Image',
                    'name' => 'image',
                    'type' => 'image',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'return_format' => 'url',
                    'preview_size' => 'medium',
                    'library' => 'all',
                    'min_width' => '',
                    'min_height' => '',
                    'min_size' => '',
                    'max_width' => '',
                    'max_height' => '',
                    'max_size' => '',
                    'mime_types' => '',
                ),
            ),
        ),
    ),
    'min' => '',
    'max' => '',
);

?>