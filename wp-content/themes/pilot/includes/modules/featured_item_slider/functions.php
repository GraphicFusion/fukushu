<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if( file_exists ( $filename )){
    require $filename;
}

function build_featured_item_slider_layout(){
    $args = array(
		'title' => get_sub_field('featured_item_slider_block_title'),
        'slider_unique_identifier' => 'slider-' . md5( time() + rand(0, 1000) ),
        'slider_logo' => get_sub_field('slider_logo'),
        'slider_content' => get_sub_field('slider_content'),
        'slides' => get_sub_field( 'slides' ),
        'slides_count' => count( get_sub_field( 'slides' ) ),
        'slider_position_class' => ( get_sub_field( 'slider_content_position' ) == 'left' ) ? 'home_box_left_content' : 'home_box_right_content'
    );
    return $args;
}
?>