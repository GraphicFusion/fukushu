<?php
	/**
	 * string	$args['title']
	 * int 		$args['slider_unique_identifier']
	 * string	$args['slider_logo']
	 * string	$args['slider_content']
	 * string	$args['slider_position_class']
	 * array 	$args['slides']
	 * int 		$args['slides_count']
	 */
	global $args;
?>
<div class="<?php echo $args[ 'slider_position_class' ]; ?>">
    <div id="<?php echo $args[ 'slider_unique_identifier' ]; ?>" class="carousel slide" data-ride="carousel">
      
		<!-- Indicators -->
		<?php if ( $args[ 'slides_count' ] ) : ?>
		<ol class="carousel-indicators">
			<?php for( $i = 0; $i < $args['slides_count']; $i++) : ?>
				<li data-target="#<?php echo $args[ 'slider_unique_identifier' ]; ?>" data-slide-to="<?php echo $i; ?>" <?php echo ( $i == 0 ) ? 'class="active"' : ''; ?>>
				</li>
			<?php endfor; ?>
		</ol>
		<?php endif; ?>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <?php
        	$is_first = true;
        	foreach ( $args[ 'slides' ] as $slide ) :
        ?>
        <div class="item <?php echo $is_first ? 'active' : ''; ?>">
          <span class="featuredslide" style="background-image: url(<?php echo $slide[ 'background_image' ]; ?>);"></span>
          <div class="featured-slide-overlay"></div>
          <div class="carousel-caption"></div>
        </div>
        <?php $is_first = false; ?>
	    <?php endforeach; ?>
        
      </div>
    </div>
    <div class="homefeatured_slider_detail">
        <img class="featured_slider_logo" src="<?php echo $args[ 'slider_logo' ]; ?>" alt="">
        <p class="featured_slider_detail"><?php echo $args[ 'slider_content' ]; ?></p>
    </div>
</div>