<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if( file_exists ( $filename )){
    require $filename;
}

function build_news_and_events_layout(){
    $args = array(
        'title' => get_sub_field( 'news_and_events_block_title' ) ? get_sub_field( 'news_and_events_block_title' ) : 'News & Events',
        'background_image' => get_sub_field( 'background_image')
    );

    $category = get_sub_field( 'category' );
    $num = get_sub_field( 'number_of_posts' );
    $posts = array();

    if ( !empty( $category) && $num > 0) {
    	$query = new WP_Query( array(
    		'cat' => $category,
    		'posts_per_page' => $num
    		)
    	);

    	if ( $query->have_posts() ) {
    		while ( $query->have_posts() ) {
    			$query->the_post();
    			$posts[] = array(
    				'title' => get_the_title(),
                    'permalink' => get_the_permalink(),
    				'category' => pilot_get_first_category(),
    				'date' => get_the_date( 'm/d/Y')
				);
    		}
    	}
        wp_reset_postdata();
    }

    $args[ 'posts' ] = $posts;

    return $args;
}
?>