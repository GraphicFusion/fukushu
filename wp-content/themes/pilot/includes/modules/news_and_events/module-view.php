<?php
	/**
	 * string	$args['title']
     * string   $args['background_image']
	 * array	$args['posts']
	 */
	global $args;
?>
<div class="restaurant_newsevents" <?php if( $args[ 'background_image' ] ) echo 'style="background-image: url( ' . $args[ 'background_image' ] . ')"'; ?>>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <a href="<?php the_permalink( get_option( 'page_for_posts' ) ); ?>" class="newevents_btn"><?php echo $args[ 'title' ]; ?></a>
            </div>
        </div>
        <div class="row">
            <?php foreach ( $args[ 'posts' ] as $post ) : ?>
            <div class="col-sm-4">
                <h3 class="newsevents_title">
                    <a href="<?php echo $post[ 'permalink' ]; ?>"><?php echo $post[ 'title' ]; ?></a>
                </h3>
                <h6 class="newsevents_date">
                    <?php echo $post[ 'category' ]; ?> - <?php echo $post[ 'date' ]; ?>
                </h6>
            </div>
            <?php endforeach; ?>
        </div>
    </div>                    
</div>