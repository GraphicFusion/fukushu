<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if( file_exists ( $filename )){
    require $filename;
}

function build_column_with_sidebar_layout(){
    $args = array(
		'title' => get_sub_field( 'column_with_sidebar_block_title' ),
        'left_column_title' => get_sub_field( 'left_column_title' ),
        'left_column_content' => get_sub_field( 'left_column_content' ),
        'right_sidebar' => get_sub_field( 'right_sidebar' )
    );

    $posts = array();

    $query = new WP_Query(array(
    	'posts_per_page' => 3
    ));

    if ( $query->have_posts() ) {
    	while ( $query->have_posts() ) {
    		$query->the_post();
    		$posts[] = array(
    			'title' => get_the_title(),
    			'permalink' => get_the_permalink(),
    			'category' => pilot_get_first_category(),
    			'date' => get_the_date( 'm/d/Y' )
    		);
    	}
    	wp_reset_postdata();
    }

    $args[ 'posts' ] = $posts;

    return $args;
}
?>