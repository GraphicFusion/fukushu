<?php
	/**
	 * string	$args['title']
	 * string	$args['left_column_title']
	 * string	$args['left_column_content']
	 * array	$args['posts']
	 */
	global $args;
?>
<div class="home_newsevents">
    <div class="container">
        <div class="row">
            <div class="col-sm-7 newsevents_left">
                <h2><?php echo $args[ 'left_column_title' ]; ?></h2>
                <?php echo $args[ 'left_column_content' ]; ?>
            </div>
            <div class="col-sm-5 newsevents_right">
                <div class="newsevents_right_inner">
                    <h5>NEWS &amp; EVENTS</h5>

                    <?php if ( count( $args[ 'posts' ] ) ) : ?>
                        <?php foreach ( $args[ 'posts' ] as $post ) : ?>
                            <h3><a href="<?php echo $post[ 'permalink' ]; ?>"><?php echo $post[ 'title' ]; ?></a></h3>
                            <h4><?php echo $post[ 'category' ]; ?> - <?php echo $post[ 'date' ]; ?></h4>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <h4>No news or events published yet.</h4>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>