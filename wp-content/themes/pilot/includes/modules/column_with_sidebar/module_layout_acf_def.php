<?php
global $pilot;
// add module layout to flexible content
$module_layout = array (
    'key' => '5900cbceef6f6',
    'name' => 'column_with_sidebar',
    'label' => 'Column With Sidebar',
    'display' => 'block',
    'sub_fields' => array (
        array (
            'key' => 'field_5900cc32ad9e2',
            'label' => 'Left Column Title',
            'name' => 'left_column_title',
            'type' => 'text',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array (
            'key' => 'field_5900ccebad9e3',
            'label' => 'Left Column Content',
            'name' => 'left_column_content',
            'type' => 'wysiwyg',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'tabs' => 'all',
            'toolbar' => 'basic',
            'media_upload' => 0,
        ),
        array (
            'key' => 'field_590a28047d4f9',
            'label' => 'News & Events',
            'name' => 'news_and_events',
            'type' => 'message',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'message' => 'Recent News & Events will be displayed here',
            'new_lines' => 'wpautop',
            'esc_html' => 0,
        ),
    ),
    'min' => '',
    'max' => '',
);

?>