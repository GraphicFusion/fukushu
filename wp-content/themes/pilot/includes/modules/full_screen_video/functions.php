<?php
	$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}
	function build_full_screen_video_layout(){
		$args = array(
			'video_url' => ( get_sub_field( 'video_source' ) == 'external' ) ? get_sub_field('external_url') : get_sub_field('local_url'),
			'video_thumbnail' => get_sub_field( 'video_thumbnail' ) ? 'poster="' . get_sub_field( "video_thumbnail" ) . '"' : '',
			'popup_url' => get_sub_field( 'popup_url' )
		);
		return $args;
	}

	
?>