<?php 
	/**
     * string   $args['video_url']
	 * string	$args['video_thumbnail']
     * string   $args['popup_url']
	 */
	global $args;
?>
<div class="video_outer">
    <video autoplay loop <?php echo !empty( $args[ 'video_thumbnail' ] ) ? $args[ 'video_thumbnail' ] : ''; ?>>
      <source src="<?php echo $args[ 'video_url' ]; ?>" type="video/mp4">
    Your browser does not support the video tag.
    </video>
    
    <?php if ( !empty($args['popup_url']) ) { ?>
        <div class="video_playbtn">
            <a href="<?php echo $args['popup_url']; ?>">
                <img src="<?php echo get_template_directory_uri() . '/image/play-button.png'; ?>">
            </a>
        </div>
    <?php } ?>

    <div class="scroll_downarrow">
        <a id="scrolldown" class="addscroll" href="javascript:void(0)" onclick="ScrollToPos('#header');">
            <img src="<?php echo wp_get_attachment_url( 85 ); ?>" alt="">
        </a>
    </div>
<?php
    if ( is_front_page() )
        get_template_part( 'views/header', 'composer' );
?>
</div>