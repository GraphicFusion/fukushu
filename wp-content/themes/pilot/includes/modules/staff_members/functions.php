<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if( file_exists ( $filename )){
    require $filename;
}

function build_staff_members_layout(){
	
    $args = array(
		'title' => get_sub_field('staff_members_block_title')
    );

    $count = get_sub_field( 'staff_num' );

    $members = array();

    $the_members = new WP_Query( array(
    	'post_type' => 'staff',
    	'posts_per_page' => $count
    ) );

    if ( $the_members->have_posts() ) :
    	while ( $the_members->have_posts() ) :
    		
    		// setup post
    		$the_members->the_post();

    		// parse posts data
    		$members[] = array(
    			'id' => get_the_ID(),
    			'title' => get_the_title(),
    			'content' => wpautop( get_the_content(), true ),
    			'thumbnail' => get_the_post_thumbnail_url( null, 'staff-thumbnail' ),
    			'role' => get_field( 'role' )
    		);

    	endwhile;

    	// restore original post
    	wp_reset_postdata();

	endif;

	// include posts in $args
	$args[ 'members' ] = $members;

    return $args;
}
?>