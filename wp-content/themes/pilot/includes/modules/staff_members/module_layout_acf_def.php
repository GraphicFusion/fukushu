<?php
global $pilot;
// add module layout to flexible content
$module_layout = array (
    'key' => '59039eed51f0f',
    'name' => 'staff_members',
    'label' => 'Staff Members',
    'display' => 'block',
    'sub_fields' => array (
        array (
            'key' => 'field_59039ef0e1c43',
            'label' => 'Number of members to show?',
            'name' => 'staff_num',
            'type' => 'number',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'min' => 1,
            'max' => '',
            'step' => '',
        ),
    ),
    'min' => '',
    'max' => '',
);

?>