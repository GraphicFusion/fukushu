$(document).ready(function() {
    // our vision gallery click function
    $( ".ourvision_gallery li" ).each(function() {
        $( this ).on("click", function(){

            $( this ).children("div").fadeOut( "overlay" );
            $( ".ourvision_gallery li div" ).not( $(this).children('div') ).fadeIn("overlay");

            // show gallery content
            var databox = $(this).attr('data-id');
            $('.visiongallery_content').fadeOut();
            $('#' + databox).fadeIn();

            // close content box
            $( ".vision_crossbtn" ).click(function(){
                $( ".visiongallery_content" ).fadeOut();
                $( ".ourvision_gallery li div" ).fadeOut();
            });     
        });
    });
});