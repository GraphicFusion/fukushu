<?php
	/**
	 * string	$args['title']
	 * array	$args['members']
	 */
	global $args;
?>
<div class="main_ourvision">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?php if ( count( $args[ 'members' ] ) ) : ?>
                <ul class="ourvision_gallery">
                    <?php foreach ( $args[ 'members' ] as $member ) : ?>
                    <li data-id="<?php echo "detailbox" . $member[ 'id' ]; ?>">
                        <img src="<?php echo $member[ 'thumbnail' ]; ?>" alt="<?php echo $member[ 'title' ]; ?>">
                        <h3><?php echo $member[ 'title' ]; ?></h3>
                        <div class="overlay"></div>
                    </li>
                    <div class="visiongallery_content" id="<?php echo "detailbox" . $member[ 'id' ]; ?>" style="display: none;">
                        <img src="<?php echo get_template_directory_uri() . '/image/cross-button.png'; ?>" alt="<?php echo $member[ 'title' ]; ?>" class="vision_crossbtn">
                        <h2><?php echo $member[ 'title' ]; ?></h2>
                        <h6><?php echo $member[ 'role' ]; ?></h6>
                        <?php echo $member[ 'content' ]; ?>
                    </div>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>