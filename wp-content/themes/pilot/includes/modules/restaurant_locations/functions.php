<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if( file_exists ( $filename )){
    require $filename;
}

function build_restaurant_locations_layout(){
    $args = array(
        'title' => get_sub_field( 'grid_boxes_block_title' ),
        'layout' => get_sub_field( 'layout' ),
        'locations' => ''
    );

    $locations_query = new WP_Query( array(
    	'post_type' => 'location',
    	'posts_per_page' => -1,
        'meta_key' => 'restaurant',
    	'meta_query' => array(
    		'key' => 'restaurant',
    		'value' => array( get_the_ID() ),
            'compare' => 'IN'
    	)	
	) );

    $locations = array();

    if ( $locations_query->have_posts() ) :
    	while ( $locations_query->have_posts() ) : $locations_query->the_post();
    		$locations[] = array(
    			'location_title' => get_the_title(),
    			'location_permalink' => get_the_permalink(),
                'location_thumbnail' => get_the_post_thumbnail_url(),
    			'location_one_liner' => get_field( 'one_liner', get_the_ID() )
    		);
	    endwhile;
        wp_reset_postdata();
    endif;

    $args[ 'locations' ] = $locations;

    return $args;
}
?>