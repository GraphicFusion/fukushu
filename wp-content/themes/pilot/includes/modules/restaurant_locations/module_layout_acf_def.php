<?php
global $pilot;
// add module layout to flexible content
$module_layout = array (
    'key' => '592d723d51671',
    'name' => 'restaurant_locations',
    'label' => 'Restaurant Locations',
    'display' => 'block',
    'sub_fields' => array (
        /*array (
            'key' => 'field_592d724689fa9',
            'label' => 'Restaurant Locations',
            'name' => '',
            'type' => 'message',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'message' => 'No options required. All the locations associated with this restaurant will be displayed.',
            'new_lines' => 'wpautop',
            'esc_html' => 0,
        ),*/
        array (
            'key' => 'field_593058516a00d',
            'label' => 'Layout Style',
            'name' => 'layout',
            'type' => 'select',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'choices' => array (
                'layout1' => 'Layout 1',
                'layout2' => 'Layout 2',
            ),
            'default_value' => array (
                0 => 'layout1',
            ),
            'allow_null' => 0,
            'multiple' => 0,
            'ui' => 0,
            'ajax' => 0,
            'return_format' => 'value',
            'placeholder' => '',
        )
    ),
    'min' => '',
    'max' => '',
);

?>