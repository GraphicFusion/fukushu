<?php
	/**
	 * string	$args['title']
	 * array	$args['locations']
	 */
	global $args;
?>
<?php if ( $args[ 'layout' ]  == 'layout1' ) : ?>
<div class="restaurantintro_outer locations_layout_1">
    <div class="container">
        <div class="row">
            <?php foreach ( $args[ 'locations' ] as $location ) : ?>
            <div class="col-sm-6">
                <div class="dining_detail">
                    <a href="<?php echo $location[ 'location_permalink' ]; ?>">
                    <img src="<?php echo $location[ 'location_thumbnail' ]; ?>" title="<?php echo $location[ 'location_title' ]; ?>" alt="<?php echo $location[ 'location_title' ]; ?>" class="dining_thumbnail img-responsive">
                    <div class="diningdetail_row">
                        <h4 class="pull-left"><?php echo $location[ 'location_title' ]; ?></h4>
                        <a href="<?php echo $location[ 'location_permalink' ]; ?>" class="diningarrow_btn pull-right">
                            <svg version="1.1" id="right_arrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40.75px" height="13.75px" viewBox="0 0 40.75 13.75" enable-background="new 0 0 40.75 13.75" xml:space="preserve">
                                <g>
                                    <rect x="0.875" y="5.875" fill="currentColor" width="37.686" height="1.918"/>
                                    <polyline fill="none" stroke="currentColor" stroke-width="2" stroke-miterlimit="10" points="33.5,0.877 39.121,6.875 33.5,12.874"/>
                                </g>
                            </svg>
                        </a>
                        <div class="clearfix"></div>
                    </div>
                    </a>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>                
</div>
<?php elseif ( $args[ 'layout' ] == 'layout2' ) : ?>
<div class="restaurantintro_outer locations_layout_2">
    <div class="container">
        <div class="row">
            <?php foreach ( $args[ 'locations' ] as $location ) : ?>
            <div class="col-sm-6">
                <div class="dinning_detail">
                    <img src="<?php echo $location[ 'location_thumbnail' ]; ?>" alt="<?php echo $location[ 'location_title' ]; ?>" class="dinning_thumbnail img-responsive">
                    <div class="dinning_inner">
                        <div class="one-liner"><?php echo $location[ 'location_one_liner' ]; ?></div>
                        <a class="title" href="<?php echo $location[ 'location_permalink' ]; ?>"><?php echo $location[ 'location_title' ]; ?>
                            <svg version="1.1" id="right_arrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40.75px" height="13.75px" viewBox="0 0 40.75 13.75" enable-background="new 0 0 40.75 13.75" xml:space="preserve">
                                <g>
                                    <rect x="0.875" y="5.875" fill="currentColor" width="37.686" height="1.918"/>
                                    <polyline fill="none" stroke="currentColor" stroke-width="2" stroke-miterlimit="10" points="33.5,0.877 39.121,6.875 33.5,12.874"/>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php endif; ?>