<?php
	/**
	 * string	$args['title']
	 * string	$args['sub_title']
	 * array	$args['list_items']
	 */
	global $args;
?>
<div class="main_ourvision">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-9 col-md-offset-2">
                <div class="ourvision_top">
                    <?php if ( $args[ 'title' ] ) : ?>
	                    <h2><?php echo $args[ 'title' ]; ?></h2>
	                <?php endif; ?>
	                <?php if ( $args[ 'sub_title' ] ) : ?>
	                    <h6><?php echo $args[ 'sub_title' ]; ?></h6>
	                <?php endif; ?>

	                <?php if ( count( $args[ 'list_items' ] ) ) : ?>
                    <ul>
                    	<?php foreach( $args[ 'list_items' ] as $item) : ?>
                        <li>
                            <span>
                                <?php echo $item['list_item_content'] ?>
                            </span>
                        </li>
	                    <?php endforeach; ?>
                        <div class="clearfix"></div>
                    </ul>
	                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>