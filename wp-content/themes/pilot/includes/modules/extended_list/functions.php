<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if( file_exists ( $filename )){
    require $filename;
}

function build_extended_list_layout(){
    $args = array(
		'title' => get_sub_field('extended_list_block_title'),
		'sub_title' => get_sub_field('sub_title'),
		'list_items' => get_sub_field('list_items'),
    );
    return $args;
}
?>