<?php
global $pilot;
// add module layout to flexible content
$module = 'extended_list';
$module_layout = array (
    'key' => '59039b1f15d02',
    'name' => 'extended_list',
    'label' => 'Extended List',
    'display' => 'block',
    'sub_fields' => array (
        array (
            'key' => 'field_59039b21a129f',
            'label' => 'Sub Title',
            'name' => 'sub_title',
            'type' => 'text',
            'instructions' => 'If left empty, sub title will not be displayed. Same goes for the title above.',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array (
            'key' => 'field_59039b36a12a0',
            'label' => 'List Items',
            'name' => 'list_items',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'collapsed' => '',
            'min' => '',
            'max' => '',
            'layout' => 'table',
            'button_label' => 'Add Row',
            'sub_fields' => array (
                array (
                    'key' => 'field_59039b43a12a1',
                    'label' => 'List Item Content',
                    'name' => 'list_item_content',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
            ),
        ),
    ),
    'min' => '',
    'max' => '',
);

?>