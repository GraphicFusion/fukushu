<?php
	/**
	 * string	$args['title']
     * array    $args['menu_titles']
	 * array	$args['menus']
	 */
	global $args;
?>
<section id="food_menu_module" class="content">
    <div class="food_menu_outer">
        <div class="main_food_menu">
            <div class="container">
                <div class="row">
                    <aside class="food_menu_sidebar">
                        <?php if ( count( $args[ 'menu_titles' ] ) ) : ?>
                        <ul class="food_menu_list">
                            <?php foreach( $args[ 'menu_titles' ] as $key => $menu_title ) : ?>
                            <li><a href="javascript:void(0);" onClick="ScrollToPos('#<?php echo str_replace( ' ', '_', $menu_title ); ?>')"><?php echo ucwords( $menu_title ); ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                        <?php endif; ?>
                    </aside>
                    <div class="food_menu_detail">
                        <?php foreach( $args[ 'menus' ] as $menu_title => $menu ) : ?>
                        <div class="div" id="<?php echo str_replace( ' ', '_', strtolower( $menu_title ) ); ?>">
                            <h2><?php echo strtoupper( $menu_title ); ?></h2>
                                <?php foreach ( $menu[ 'sections' ] as $section ) : ?>
                                    <div class="food_menu_inner">
                                        <!-- section -->
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h3><?php echo $section[ 'title' ]; ?></h3>
                                                <?php if ( !empty( $section[ 'notes' ] ) ) : ?>
                                                    <p class="title_description"><?php echo $section[ 'notes' ]; ?></p>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="item_title_outer row">
                                            <?php $items = 1; ?>
                                            <?php foreach( $section[ 'menu_items' ] as $item ) : ?>
                                                <div class="col-sm-6">
                                                    <h4><?php echo $item[ 'title' ]; ?></h4>
                                                    <p class="menu_item_description"><?php echo $item[ 'description' ]; ?></p>
                                                    <span><?php echo $item[ 'price' ]; ?></span>
                                                </div>
                                                <?php if ( $items++ % 2 == 0 ) : ?>
                                                    </div>
                                                    <div class="item_title_outer row">
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>