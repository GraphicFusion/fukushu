<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if( file_exists ( $filename )){
    require $filename;
}

function build_menu_layout(){
    $args = array(
		'title' => get_sub_field('fancy_slider_block_title'),
		'menu_titles' => '',
        'menus' => ''
    );

    $menus = array();

    // menus
    $menus = array();
    while ( have_rows( 'menus' ) ) : the_row();

        // sections
        $sections = array();
        while ( have_rows( 'sections' ) ) : the_row();
            $section = array(
                'title' => get_sub_field( 'section_title' ),
                'notes' => get_sub_field( 'section_notes' )
            );
            
            // items
            $items = array();
        
            while ( have_rows( 'menu_items' ) ) : the_row();
                $items[] = array(
                    'title' => get_sub_field( 'item_title' ),
                    'description' => get_sub_field( 'item_description' ),
                    'price' => get_sub_field( 'item_price' )
                );
            endwhile;
        
            $section[ 'menu_items' ] = $items;
            $sections[] = $section;

        endwhile;
        
        $menus[ get_sub_field( 'title' ) ][ 'sections' ] = $sections;

    endwhile;
  
    $args[ 'menu_titles' ] = array_map('strtolower', array_keys( $menus ) );
    $args[ 'menus' ] = $menus;


    // echo "<pre>";
    // print_r( $args['menus'] );
    // die();

    return $args;
}
?>