<?php
	$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}
	function build_announcement_layout(){
		$args = array(
			'title' => get_sub_field( 'announcement_block_title' ),
			'announcement' => get_sub_field( 'announcement' ),
			'announcement_page_link' => get_permalink( get_sub_field( 'announcement_page_link' ) ),
			'background_color' => get_sub_field( 'background_color' ) ? get_sub_field( 'background_color' ) : 'initial'
		);
		return $args;
	}
?>