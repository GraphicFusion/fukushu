<?php 
	/**
	 * string	$args['title']
     * string   $args['announcement']
     * string   $args['announcement_page_link']
	 * string	$args['background_color']
	 */
	global $args;
?>
<div id="home_detail" class="announcement_outer" style="background-color: <?php echo $args['background_color']; ?>">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <span class="announcement_text"><?php echo $args['title']; ?></span>
                <h1 class="announcement_title"><a href="<?php echo $args['announcement_page_link']; ?>"><?php echo $args['announcement']; ?></a></h1>
            </div>
        </div>
    </div>
</div>