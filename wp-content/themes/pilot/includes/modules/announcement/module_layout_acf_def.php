<?php
	global $pilot;
	// add module layout to flexible content 
	$module_layout = array (
		'key' => '58ff94515527a',
		'name' => 'announcement',
		'label' => 'Announcement',
		'display' => 'block',
		'sub_fields' => array (
			array (
				'key' => 'field_58ff94686a4bd',
				'label' => 'Announcement',
				'name' => 'announcement',
				'type' => 'text',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array (
				'key' => 'field_590a3da9ba9b7',
				'label' => 'Announcement Page Link',
				'name' => 'announcement_page_link',
				'type' => 'post_object',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => array (
					0 => 'post',
				),
				'taxonomy' => array (
				),
				'allow_null' => 0,
				'multiple' => 0,
				'return_format' => 'id',
				'ui' => 1,
			),
			array (
				'key' => 'field_58ff9afc57723',
				'label' => 'Background Color',
				'name' => 'background_color',
				'type' => 'color_picker',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '#171717',
			),
		),
		'min' => '',
		'max' => '',
	);
?>