<?php 
	/**
     * string   $args['banner_background_image']
     * string   $args['banner_title']
	 * string	$args['banner_title_allignment']
	 */
	global $args;
?>
<section class="vision_heroimg" style="background-image: url(<?php echo $args[ 'banner_background_image' ]; ?>);">
    <div class="vision-hero-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 style="text-align: <?php echo $args[ 'banner_title_alignement' ]; ?>;"><?php echo $args[ 'banner_title' ]; ?></h1>
            </div>
        </div>
    </div>
</section>