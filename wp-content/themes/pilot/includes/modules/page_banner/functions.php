<?php
	$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}
	function build_page_banner_layout(){
		$args = array(
			'banner_background_image' => get_sub_field( 'banner_background_image' ),
			'banner_title' => get_sub_field( 'banner_title' ),
			'banner_title_alignement' => get_sub_field( 'banner_title_alignment' )
		);
		return $args;
	}
?>