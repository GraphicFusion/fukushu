<?php
	/**
	 * string	$args['title']
	 * array	$args['items']
	 */
	global $args;
?>
<div class="home_btm">
	<?php foreach( $args[ 'items' ] as $item ) : ?>
    <div class="btm_box1 <?php echo $item[ 'grid_position_class' ]; ?>" style="background-image: url(<?php echo $item[ 'grid_background_image' ]; ?>);">
	    <div class="logo_box">

	        <img src="<?php echo $item[ 'grid_logo' ]; ?>">
    
	        <?php echo $item[ 'grid_content' ]; ?>
	        <?php if ( $item[ 'grid_has_link' ] ) : ?>
	        <a href="<?php echo $item[ 'grid_link_url' ]; ?>"><?php echo $item[ 'grid_link_text' ]; ?></a>
		    <?php endif; ?>
	    </div>
    </div>
    
    <?php if ( $item[ 'grid_position_class' ] == 'pull-right' ) : ?>
    <div class="clearfix"></div>
	<?php endif; ?>

	<?php endforeach; ?>
</div>