<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if( file_exists ( $filename )){
    require $filename;
}

function build_grid_boxes_layout(){
    $args = array(
		'title' => get_sub_field( 'grid_boxes_block_title' )
    );

    // preapre grid items
    $items = array();
    $grid_items = get_sub_field( 'grid_items' );
    $i = 1;
    
    foreach ( $grid_items as $grid_item ) {
    	$items[] = array(
    		'grid_position_class' => ( ($i++ % 2) == 0 ) ? 'pull-right' : 'pull-left',
    		'grid_background_image' => $grid_item[ 'grid_background_image' ],
    		'grid_logo' => $grid_item[ 'grid_logo' ],
    		'grid_content' => $grid_item[ 'grid_content' ],
    		'grid_has_link' => !empty( $grid_item[ 'grid_link_text' ] ),
    		'grid_link_text' => $grid_item[ 'grid_link_text' ],
    		'grid_link_url' => $grid_item[ 'grid_link_url' ]
    	);
    }

    $args[ 'items' ] = $items;

    return $args;
}
?>