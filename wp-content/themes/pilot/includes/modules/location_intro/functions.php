<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if( file_exists ( $filename )){
    require $filename;
}

function build_location_intro_layout(){
    $args = array(
		'title' => get_sub_field('location_intro_block_title'),
		'description' => get_sub_field('description'),
		'address' => get_sub_field('address'),
		'hours' => get_sub_field('hours'),
		'phone' => get_sub_field('phone')
    );
    return $args;
}
?>