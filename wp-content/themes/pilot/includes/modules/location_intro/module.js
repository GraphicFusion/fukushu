jQuery( window ).ready(function(){
    // food menu scroll
    $(".photos_list").click(function() {
        $('html,body').animate({
            scrollTop: $(".slider-block").offset().top},
            1000);
    });

    $(".menus_list").click(function() {
        $('html,body').animate({
            scrollTop: $(".main_food_menu").offset().top},
            1000);
    });
});

function ScrollToPos(target,act){
    if( target == "#" ){
       $( 'html, body' ).animate({ scrollTop: 0 }, 1000);
      } else {
       $( 'html, body' ).animate({ scrollTop: $(target).offset().top }, 1000);
    }   
}