<?php
global $pilot;
// add module layout to flexible content
$module_layout = array (
    'key' => '59233c0f2b2a5',
    'name' => 'location_intro',
    'label' => 'Location Intro',
    'display' => 'block',
    'sub_fields' => array (
        array (
            'key' => 'field_59233c16b0c11',
            'label' => 'Description',
            'name' => 'description',
            'type' => 'wysiwyg',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'tabs' => 'all',
            'toolbar' => 'full',
            'media_upload' => 0,
        ),
        array (
            'key' => 'field_59233c6eb0c12',
            'label' => 'Address',
            'name' => 'address',
            'type' => 'textarea',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'maxlength' => '',
            'rows' => '3',
            'new_lines' => 'wpautop',
        ),
        array (
            'key' => 'field_59233c80b0c13',
            'label' => 'Hours',
            'name' => 'hours',
            'type' => 'textarea',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'maxlength' => '',
            'rows' => '',
            'new_lines' => 'wpautop',
        ),
        array (
            'key' => 'field_59233cacb0c14',
            'label' => 'Phone',
            'name' => 'phone',
            'type' => 'text',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'min' => '',
    'max' => '',
);

?>