<?php
	/**
	 * string	$args['title']
	 * string	$args['description']
	 * string	$args['address']
	 * string	$args['hours']
	 * string	$args['phone']
	 */
	global $args;
	$restaurant_id = get_field( 'restaurant' )->ID;
?>
<div class="food_menu_outer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="food_menu_title"><?php the_title(); ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="location_detail">
                <?php echo $args[ 'description' ]; ?>
                <ul class="menu_title_list">
                    <li class="photos_list">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="22.75px" height="13.75px" viewBox="0 0 18.75 13.75" enable-background="new 0 0 18.75 13.75" xml:space="preserve">
                            <g>
                                <rect x="0.875" y="5.875" fill="currentColor" width="17.686" height="1.918"/>
                                <polyline fill="none" stroke="currentColor" stroke-width="2" stroke-miterlimit="10" points="13.5,0.877 19.121,6.875 13.5,12.874"/>
                            </g>
                        </svg>
                        Photos
                    </li>
                    <li class="menus_list">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="22.75px" height="13.75px" viewBox="0 0 18.75 13.75" enable-background="new 0 0 18.75 13.75" xml:space="preserve">
                            <g>
                                <rect x="0.875" y="5.875" fill="currentColor" width="17.686" height="1.918"/>
                                <polyline fill="none" stroke="currentColor" stroke-width="2" stroke-miterlimit="10" points="13.5,0.877 19.121,6.875 13.5,12.874"/>
                            </g>
                        </svg>
                        Menus
                    </li>
                </ul>
            </div>

            <div class="location_address">
                <h3><?php echo get_the_title( $restaurant_id ) . ' ' . get_the_title(); ?></h3>
                
                <h4>Address</h4>
                <?php the_sub_field( 'address' ); ?>
                
                <h4>Hours</h4>
                <?php the_sub_field( 'hours' ); ?>

                <h4>phone</h4>
                <p class="phone_no"><?php the_sub_field( 'phone' ); ?></p>
            </div>
        </div>

    </div>
</div>