<?php
function pilot_setup() {

	load_theme_textdomain( 'pilot', get_template_directory() . '/languages' );

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );

	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'pilot' ),
		'restaurant-primary' => esc_html__( 'Restaurant Primary', 'pilot' ),
		'restaurant-secondary' => esc_html__( 'Restaurant Secondary', 'pilot' ),
	) );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
}
add_action( 'after_setup_theme', 'pilot_setup' );

/**
 * Register widget area.
 */
function pilot_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'pilot' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Blog Page Sidebar', 'pilot' ),
		'id'            => 'sidebar-blog-page',
		'description'   => 'This sidebar area is styled for Search and Category widgets only.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Left Sidebar', 'pilot' ),
		'id'            => 'footer-left-sidebar',
		'description'   => 'This sidebar area is styled for Custom Nav Menu widget only.',
		'before_widget' => '<div id="%1$s" class="widget %2$s col-xs-4">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="widget-title">',
		'after_title'   => '</h6>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Middle Sidebar', 'pilot' ),
		'id'            => 'footer-middle-sidebar',
		'description'   => 'This sidebar area is styled for Custom Nav Menu widget only.',
		'before_widget' => '<div id="%1$s" class="widget %2$s col-xs-4">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="widget-title">',
		'after_title'   => '</h6>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Right Sidebar', 'pilot' ),
		'id'            => 'footer-right-sidebar',
		'description'   => 'This sidebar area is styled for Custom Nav Menu widget only.',
		'before_widget' => '<div id="%1$s" class="widget %2$s col-xs-4">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="widget-title">',
		'after_title'   => '</h6>',
	) );	
}
global $pilot;
// see global var $pilot_sidebar in functions.php to turn on sidebar
if( $pilot->sidebar ){
	add_action( 'widgets_init', 'pilot_widgets_init' );
}

add_action('after_switch_theme', 'pilot_setup_options');
function pilot_setup_options () {
	require get_template_directory() . '/includes/pilot-theme-activation.php';
}

add_action('switch_theme', 'pilot_destroy_options');
function pilot_destroy_options () {
	require WP_CONTENT_DIR . '/themes/pilot/includes/pilot-theme-deactivation.php';
}

/* Hide ACF by Default (turn on in pilot-superadmin-settings.php) */
// if( is_admin() ){
// 	add_filter('acf/settings/show_admin', function(){ return false; });
// }

// image sizes
add_image_size( 'staff-thumbnail', 285, 285, true);
add_image_size( 'index-thumbnail', 790, 527, true);
add_image_size( 'related-thumbnail', 585, 389, true);

// allow svg upload
function cc_mime_types( $mimes ){
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

function filter_ptags_on_images($content) {
    return preg_replace('/<p>(\s*)(<img .* \/>)(\s*)<\/p>/iU', '\2', $content);
}
add_filter('the_content', 'filter_ptags_on_images');

// custom body classes for restaurants
function my_body_classes( $classes ) {
 
 	if ( get_post_type() == 'restaurant' ) {
 		
 		$custom_class = '';

 		if ( strpos( strtolower( get_the_title() ), 'obon') !== false ) {
 			$custom_class = 'restaurant_obon_body';
 		} elseif ( strpos( strtolower( get_the_title() ), 'goodness') !== false ) {
 			$custom_class = 'restaurant_goodness_body';
 		} elseif ( strpos( strtolower( get_the_title() ), 'bird') !== false ) {
 			$custom_class = 'restaurant_bird_body';
 		} elseif ( strpos( strtolower( get_the_title() ), 'duck') !== false ) {
 			$custom_class = 'restaurant_duckncover_body';
 		}

    	$classes[] = $custom_class;
 	}

    return $classes;
     
}
add_filter( 'body_class','my_body_classes' );