<?php

function ajax_posts_by_category() {

	$output = array();

	if ( $_POST[ 'term' ] AND !empty( $_POST[ 'term' ])  ) {

		$cat = $_POST[ 'term' ];
		
		$posts = new WP_Query(array(
			'category_name' => $cat,
			'posts_per_page' => -1
		));

		if ( $posts->have_posts() ) {
			while ( $posts->have_posts() ) :
				$posts->the_post();
				$output[] = array(
					'title' => get_the_title(),
					'permalink' => get_the_permalink(),
					'thumbnail' => get_the_post_thumbnail_url( get_the_ID(), 'index-thumbnail' ),
					'category' => pilot_get_first_category(),
					'date' => get_the_date( 'm/d/Y' )
				);
			endwhile;

			echo json_encode( $output );
		} else {
			echo json_encode( 'no-category-results' );
		}

	} else {
		echo json_encode( 'No or invalid parameters provided.' );
	}

	die();
}
add_action( 'wp_ajax_nopriv_ajax_posts_by_category', 'ajax_posts_by_category' );
add_action( 'wp_ajax_ajax_posts_by_category', 'ajax_posts_by_category' );


// AJAX search term action
function ajax_posts_by_search_term() {

	$output = array();

	if ( $_POST[ 'term' ] AND !empty( $_POST[ 'term' ])  ) {

		$term = $_POST[ 'term' ];
		
		$posts = new WP_Query(array(
			's' => $term,
			'posts_per_page' => -1
		));

		if ( $posts->have_posts() ) {
			while ( $posts->have_posts() ) :
				$posts->the_post();
				$output[] = array(
					'title' => get_the_title(),
					'permalink' => get_the_permalink(),
					'thumbnail' => get_the_post_thumbnail_url( get_the_ID(), 'index-thumbnail' ),
					'category' => pilot_get_first_category(),
					'date' => get_the_date( 'm/d/Y' )
				);
			endwhile;

			echo json_encode( $output );
		} else {
			echo json_encode( 'no-search-results' );
		}

	} else {
		echo json_encode( 'No or invalid parameters provided.' );
	}

	die();
}
add_action( 'wp_ajax_nopriv_ajax_posts_by_search_term', 'ajax_posts_by_search_term' );
add_action( 'wp_ajax_ajax_posts_by_search_term', 'ajax_posts_by_search_term' );