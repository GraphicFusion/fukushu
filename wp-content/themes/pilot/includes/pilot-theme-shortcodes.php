<?php

// Restaurant Logos Shortcode
function pilot_restaurant_logos_shortcode( $atts ) {

	// Attributes
	extract(shortcode_atts(
		array(
			'num' => -1,
		),
		$atts
	));

	$logos = new WP_Query( array(
		'post_type' => 'restaurant',
		'posts_per_page' => $num
		)
	);

	$output = '';

	if ( $logos->have_posts() ) :
	
		$output .= '<ul class="modalbox_logos">';

		$i = 1;
		
		while ( $logos->have_posts() ) :
			
			$logos->the_post();
		
			$output .= '<li>';
			$output .= '<a href="' . get_the_permalink() . '"><img src="' . get_the_post_thumbnail_url() . '" alt="' . get_the_title() . '"></a>';
			$output .= '</li>';
	
			/*if ( $i++ % 2 == 0 ) {
				$output .= '<div class="clearfix"></div>';
			}*/

		endwhile;
		
		$output .= '</ul>';

		wp_reset_postdata();

	endif;

	return $output;

}
add_shortcode( 'pilot_restaurant_logos', 'pilot_restaurant_logos_shortcode' );