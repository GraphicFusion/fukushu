<?php get_header(); ?>

<!-- content -->
<section class="content">
    <div class="careers_detail_outer">
        <div class="inner">

        	<?php while ( have_posts() ) : the_post(); ?>
        		<?php get_template_part( 'views/content', 'career' ); ?>
            <?php endwhile; ?>

            <?php // get_template_part( 'views/related', 'single' ); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>