<?php get_header(); ?>

<!-- content -->
<section class="content">
    <div class="newspost_outer">
        <div class="container">

        	<?php while ( have_posts() ) : the_post(); ?>
        		<?php get_template_part( 'views/content', 'page' ); ?>
            <?php endwhile; ?>

        </div>
    </div>
</section>

<?php get_footer(); ?>