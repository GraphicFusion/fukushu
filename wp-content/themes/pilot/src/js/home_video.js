// home video height
function videoHeight() {
    // home video height
    var width = window.innerWidth;
    var windowHeight = $(window).height();
    var headerHeight = $(".main_header").height();
    var ratio = width/windowHeight;

    $(".video_outer").css("height", windowHeight);

    if (ratio > 1.7777778 ) {
        $('.video_outer video').width( $( window ).width() ).height('auto');
    } else {
        $('.video_outer video').height( windowHeight ).width('auto');
    }

    // var header = $( '.main_header' );
    // header.css( 'top', $( window ).height() - 80 );
}

var scrollFunc;
var isAnimating = false;

// scrolltop nav
function ScrollToPos(target, act) {
    var top = $(target).offset().top,
    isAnimating = true;

    function animateFalse() { isAnimating = false;}
    if (target == "#header") {
        $(".main_header").animate({"top": $( window ).height() - 80 }, 1000);
        $('html,body').animate({ scrollTop: top }, 1000, animateFalse);
    } else {

        if ( target == '#home_detail' ) {
            $(".main_header").animate({"top":"0px"}, 1000);
        }

        $('html,body').animate({ scrollTop: top - 80 }, 1000, animateFalse);
    }
}

jQuery(document).ready(function( $ ) {

    if ( $( 'body').hasClass( 'home' ) ) {
        //var countoffset = $(".announcement_outer").offset().top;

        $(".video_playbtn a").magnificPopup({
            type: 'iframe'
        });

        // calling home video height function
        videoHeight();

        $(window).resize(function() {
            videoHeight();
        });

        $(window).scroll(windowScroll);
    }
});

$("body.home #scrolldown").click(function() {
    $(this).removeClass('addscroll');
});

$("body.home .navbar-brand").click(function() {
    $("#scrolldown").addClass('addscroll');
    $("#scrolldown").removeClass('downContent');
    $(window).unbind('scroll');
    $(window).scroll(windowScroll);
});

function windowScroll(e) {
    stickyHeaderLogic();

    if ($(window).scrollTop() > 300) {
        // $(".main_header").addClass("fixed");
        // $(".main_header").animate({"top":"0px"}, "fast");
    } else {
        //remove the background property so it comes transparent again (defined in your css)
        $(".main_header").removeClass("fixed");
    }

    // if ( isAnimating ) return;
    // var countoffset = $(".announcement_outer").offset().top;
    // var scrollDown = $("#scrolldown");
    // var top = $(window).scrollTop();
    // if (top >= 100 && scrollDown.hasClass('addscroll')) {
    //     $(window).scrollTop(100);
    // } else if (!scrollDown.hasClass('addscroll') && !scrollDown.hasClass('downContent')) {
    //     scrollDown.addClass('downContent');
    // } else if (top <= countoffset - 80 && scrollDown.hasClass('downContent')) {
    //     $(window).scrollTop(countoffset - 80);
    // }
}

function stickyHeaderLogic() {
    var headerRect = $('#header').get(0).getBoundingClientRect();

    console.log('Top is: ' + headerRect.top);
    console.log('Window height is: ' + parseInt( $(window).height()-80 ));

    if ( $(window).height()-80 <= $(window).scrollTop() ) {
        $( '#header' ).addClass( 'stick' );
    } else {
        $( '#header' ).removeClass( 'stick' );
    }
}