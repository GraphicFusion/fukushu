(function() {

    wow = new WOW({
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        offset:       0,          // default
        mobile:       true,       // default
        live:         true        // default
    });
    wow.init();

})();

jQuery(document).ready(function($) {
    // Initiate magnific

    $(".mfp-media a").magnificPopup({
        type:'iframe',
        closeMarkup: '<button title="%title%" type="button" class="mfp-close"></button>'
    });

    $(".mfp-slide").magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        image: {
            verticalFit: true
        }
    });

    // Initiate Slick

    $(".multi-image-slider").slick({
        centerMode: true,
        slidesToShow: 1,
        speed: 300,
        easing: 'swing',
        arrows: true,
        autoplay: true,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    centerMode: true,
                    centerPadding: '40px',
                }
            },
            {
                breakpoint: 480,
                settings: {
                    centerMode: true,
                    centerPadding: '40px',
                }
            }
        ]
    });

    // hook .restaurants-menu-pop to pop up restaurants pop up
    $( '.restaurants-menu-pop' ).click( function( evt ){
        evt.preventDefault();
        $( '.bs-example-modal-lg' ).modal( 'show' );
    } );

    // restaurant nav function
    $( ".restaurant_heroimg .sub_navbtn, .single-location .sub_navbtn" ).click(function(){
        $( ".sub_nav" ).addClass( "show_nav" );
        $( "body" ).css( "overflow", "hidden");
    });

    $( ".sub_nav img.nav_crossbtn" ).click(function(){
        $( ".sub_nav" ).removeClass( "show_nav" );
        $( "body" ).css( "overflow-y", "scroll");
    });

    $(".sub_nav").on('blur',function(){
        $(this).fadeOut(300);
        if ($(window).width() < 640) {
            $( "body" ).css( "overflow", "scroll");
        }
    });

    // closing nav on outsidenav click
    $(document).mouseup(function (e) {
         var popup = $(".sub_nav");
         if (!$('.sub_nav img.nav_crossbtn').is(e.target) && !popup.is(e.target) && popup.has(e.target).length == 0) {
            if ($(window).width() < 640) {
                $( "body" ).css( "overflow", "scroll");
            }
             popup.removeClass( "show_nav" );
         }
     });

    if ( isMobile.any() ) {
        $('.carousel').carousel({
            interval: false
        }).find( '.carousel-indicators').hide();
    }

    // 

});

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

function ScrollToPosNormal( target, act ){
    if( target == "#" ){
       jQuery('html,body').animate({ scrollTop: 0 }, 1000);
      } else {
       jQuery('html,body').animate({ scrollTop: $(target).offset().top }, 1000);
    }   
}

jQuery( window ).load( function(){
    var $ = jQuery;

    var windowWidth = $( window ).width();
    
    if ( ( windowWidth > 767 ) && $( '#food_menu_module' ).length ) {

        var offsetChange = 0;

        if ( windowWidth >= 1440 )
            offsetChange = 400;
        else if ( windowWidth >= 1024 )
            offsetChange = 250;
        else if ( windowWidth >= 768 )
            offsetChange = 100;

        var menu = $('#food_menu_module'),
            menuStart = $( '.food_menu_list' ).offset().top - 50;
        var menuEnd = menu.closest( '.block-menu' ).get(0).getBoundingClientRect().bottom + window.scrollY,
            scrollPos,
            rect;

        console.log("menuStart = " + menuStart);
        console.log("menuEnd = " + menuEnd);

        $( window ).scroll( function() {
            scrollPos = $( window ).scrollTop();
            
            if ( menuStart < scrollPos && menuEnd > scrollPos ) {
                
                $( '.food_menu_sidebar ul' ).css({ position: 'relative', top: 'auto' });

                rect = $('.food_menu_list').get(0).getBoundingClientRect();

                if ( rect.top < 50 ) {
                    $( '.food_menu_sidebar ul' ).css({ position: 'fixed', top: '50px' });
                    // console.log( 'Stick v1.93' );
                } else if ( rect.top > 50 ) {
                    $( '.food_menu_sidebar ul' ).css({ position: 'relative', top: 'auto' });
                    // console.log( 'back to normal' );
                }
            } else if ( menuStart > scrollPos || scrollPos > menuEnd ) {
                // console.log( 'back to normal' );
                $( '.food_menu_sidebar ul' ).css({ position: 'relative', top: 'auto' });
            }
        } );
    }
} );