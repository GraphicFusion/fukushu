var loadingInterval;

jQuery( window ).ready(function() {

	// category filter
    jQuery('.newsevents_left .widget_categories ul li a').click(function( e ) {
        e.preventDefault();

        jQuery( '#ajax-loading' ).empty();

        var catLink = this.href;

        if (catLink.lastIndexOf('/') == catLink.length - 1) {
            catLink = catLink.replace( /.$/, '' )
        }

        var catArray = catLink.split('/');
        var cat = catArray[catArray.length - 1];

        jQuery( '#ajax-loading' ).append( '<span>Loading posts for <i>"' + cat + '"</i></span>' );

        // prepare data for AJAX request
        var data = {
        	action     : 'ajax_posts_by_category',
        	term        : cat,
        	filterType : 'category'
        };

        // send AJAX request
        ajaxRequest( data );
    });

    // search filter
    jQuery('.newsevents_left .widget_search #blog-search').submit(function( e ) {
        e.preventDefault();

        jQuery( '#ajax-loading' ).empty();

       	var term = $( e.currentTarget ).find( '.search_field' ).val().trim();

        if ( term.length == 0 ) {
    		return false;
        }

        jQuery( '#ajax-loading' ).append( '<span>Searching for <i>"' + term + '"</i></span>' );

        // prepare data for AJAX request
        var data = {
        	action     : 'ajax_posts_by_search_term',
        	term        : term,
        	filterType : 'search'
        };

        // send AJAX request
        ajaxRequest( data );
    });
});

function ajaxRequest( data ) {
    jQuery.ajax({
        // url      : '/wp-admin/admin-ajax.php',
    	url      : ajax.ajaxurl,
    	type     : 'post',
    	dataType : 'json',
    	data     : data,
    	beforeSend: function(){
	        jQuery( '.post_detail' ).slideUp( 500, function() {
				jQuery( '#ajax-loading' ).fadeIn();
	        } );
	    },
    	success  : function( response ) {
        	setTimeout( function(){
        		renderPosts( data, response );
        	}, 3000 );
        }
    });
}

function renderPosts( data, posts ) {
	
	jQuery( '#posts-container' ).empty();

	var output = '';

	jQuery.each( posts, function( index, post ){
		output = '<div class="post_box"><a href="' + post.permalink + '"><img src="' + post.thumbnail + '" alt="' + post.title + '" class="img-responsive"><div class="post_inner"><h4>' + post.category + ' - ' + post.date + '</h4><h3>' + post.title + '</h3></div></a></div>';

		jQuery( '#posts-container' ).append( output );
	});

	if ( data.filterType == 'category' ) {
		jQuery( '#ajax-loading' ).html( '<span>News & Events in <i>"' + data.term + '"</i></span>');
	} else if ( data.filterType == 'search' ) {
		jQuery( '#ajax-loading' ).html( '<span>Search results for <i>"' + data.term + '"</i></span>');
	}
	jQuery( '#posts-container' ).slideDown( 500 );
}