<?php get_header(); ?>

<!-- content -->
<section class="content">
    <div class="newsevents_outer">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <aside class="newsevents_left">
                        <?php dynamic_sidebar( 'sidebar-blog-page' ); ?>
                    </aside>
                </div>

                <div class="col-sm-8">
                    
                    <div id="ajax-loading">
                        <span>Filtering the taste. Please wait<span class="loading-dots"></span></span>
                    </div>

                    <div id="posts-container" class="post_detail">


					<?php if ( have_posts() ) : ?>

						<?php while ( have_posts() ) : the_post(); ?>

							<?php get_template_part( 'views/content', 'index' ); ?>
						
                        <?php endwhile; ?>

                        <?php // the_posts_navigation(); ?>
                        
                        <div class="posts-navigation">
                            <div class="nav-previous">
                                <?php next_posts_link( 'Older posts' ); ?>
                            </div>
                            
                            <div class="nav-next">
                                <?php previous_posts_link( 'Newer posts' ); ?>
                            </div>
                        </div>

                    <?php else : ?>

                        <?php get_template_part( 'views/content', 'none' ); ?>
                    
                    <?php endif; ?>

					</div>

				</div>
            </div>                    
        </div>
    </div>
</section>

<?php get_footer(); ?>