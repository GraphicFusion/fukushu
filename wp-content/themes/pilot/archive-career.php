<?php get_header(); ?>

    <section class="content">
	    <div class="careers_detail_outer">
	        <div class="careers_inner">
	            <div class="table_outer table-responsive">
	                <table class="table">
	                    <tr class="table-header">
	                        <th>POSITION</th>
	                        <th>ESTABLISHMENT</th>
	                        <th>CITY, STATE</th>
	                        <th></th>
	                    </tr>

	                    <?php
	                    	// manipulating posts per page
		                    query_posts( array(
		                    	'post_type' => 'career',
		                    	'posts_per_page' => -1,
		                    	'paged' => $paged
		                    ) );
	                    ?>

						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'views/loop', 'career' ); ?>
						<?php endwhile; wp_reset_query(); ?>
					
					</table>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>